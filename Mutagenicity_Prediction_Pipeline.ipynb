{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Machine-learning based Mutagenicity prediction pipeline #"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd \n",
    "import numpy as np\n",
    "import scripts.machine_learning as ML\n",
    "\n",
    "from sklearn.preprocessing import minmax_scale\n",
    "\n",
    "from scripts.chem_tools import batch_molFP\n",
    "from scripts.metrics import basic_metrics\n",
    "from scripts.utils import export_data\n",
    "\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interactive, fixed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Loading curated Hansen SMILES\n",
    "\n",
    "The Hansen mutagenicity benchmark dataset was downloaded from the [lab's website](http://doc.ml.tu-berlin.de/toxbenchmark/index.html#v2) and was curated through multiple processes:\n",
    "1. Removing molecules that do not have identifiers, SMILES or mutagenicity information\n",
    "2. Removing molecules that can not be processed by RDkit\n",
    "3. SMILES standardization by: \n",
    "    * remove explicit hydrogens \n",
    "    * disconnect metals\n",
    "    * normalize molecule (correct functional groups and recombine charges)\n",
    "    * re-ionize molecule\n",
    "    * keep the largest fragment\n",
    "    * canonicalize SMILES code\n",
    "4. Removing duplicates based on identifiers and SMILES code using weight-of-evidence rule, i.e. favoring mutagens over non-mutagens\n",
    "5. Removing compounds not processed by Mordred\n",
    "\n",
    "Mutagenicity outcomes are binarized with `0` being non-mutagens and `1` as mutagens."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dataset = pd.read_csv('data/input/Hansen_metadata.csv')\n",
    "dataset = dataset.sample(frac=1).reset_index(drop=True)\n",
    "display(dataset['Mutagenicity'].value_counts())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Generating structure-based representations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fingerprint_names = ['ECFP4_256', 'ECFP6_256', 'ECFP4_512', 'ECFP6_512', 'MACCS', 'mol2vec']\n",
    "\n",
    "fingerprints = {}\n",
    "for fp in fingerprint_names :\n",
    "    algo = fp\n",
    "    if 'ECFP' in algo : \n",
    "        algo, parameters = 'ECFP', {'radius' : int(fp[4]), 'length' : int(fp.split('_')[1])}\n",
    "        fingerprints[fp] = batch_molFP(dataset['SMILES'].values, algo, **parameters)\n",
    "    else :\n",
    "        fingerprints[fp] = batch_molFP(dataset['SMILES'].values, algo)\n",
    "\n",
    "fingerprints = pd.DataFrame.from_dict(fingerprints, orient='columns')\n",
    "fingerprints.index = dataset['SMILES'].values\n",
    "\n",
    "fingerprints = fingerprints.loc[~fingerprints.isnull().any(axis=1)]\n",
    "\n",
    "# Adding pre-calculated GCN vectors\n",
    "GCN_vectors = pd.read_parquet('data/input/GCN_Vectors.parquet')\n",
    "fingerprints = fingerprints.merge(GCN_vectors[['GCN_vectors']], right_index=True, left_index=True)\n",
    "fingerprints['GCN_vectors'] = minmax_scale(np.stack(fingerprints['GCN_vectors'].values), axis=1).tolist()\n",
    "fingerprints['mol2vec'] = minmax_scale(np.stack(fingerprints['mol2vec'].values), axis=1).tolist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Adding Mordred descriptors\n",
    "\n",
    "Mordred descriptors were calculated in the [here](here) Jupyter Notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mol_properties = pd.read_parquet('data/input/Hansen_Molecular_Properties.parquet')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Adding genotoxocity-related descriptors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SA_Alerts = pd.read_parquet('data/input/Hansen_SA.parquet')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Energy-based descriptors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "DFT_features = pd.read_parquet('data/input/Hansen_DFT.parquet')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Training and assessing the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ml_pipeline = interactive(ML.pipeline_prediction,\n",
    "                         {'manual' : True, 'manual_name' : 'Generate Model'},\n",
    "                         data=fixed((dataset, fingerprints, mol_properties, SA_Alerts, DFT_features)),\n",
    "                         algorithm=widgets.Dropdown(options=['SVM','RF','NBC','DNN'], description='Algorithm:'), \n",
    "                         mol_FP=widgets.Dropdown(options=['ECFP4_256', 'ECFP4_512', 'ECFP6_256', 'ECFP6_512', 'MACCS', 'mol2vec', 'GCN_vectors', False], description='Representation:'),\n",
    "                         mol_properties=widgets.Dropdown(options=[True, False], description='Properties:'), \n",
    "                         SA_features=widgets.Dropdown(options=[True, False], description='Structural Alert:'), \n",
    "                         DFT_features=widgets.Dropdown(options=[True, False], description='DFT:'), \n",
    "                         CV_fold=fixed(5))\n",
    "display(ml_pipeline) ;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 7. Export Results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exporting = interactive(export_data, {'manual' : True, 'manual_name' : 'Export'},\n",
    "                        data=fixed(ml_pipeline.result),\n",
    "                        pred_path=widgets.Text(value=f\"data/output/Predictions/predictions.csv\", placeholder='Location of PCA image', description='Prediction:'),\n",
    "                        metric_path=widgets.Text(value=f\"data/output/Results/metric.csv\", placeholder='Location of PCA image', description='Metric:'),\n",
    "                        figure_path=widgets.Text(value=f\"data/output/ROC_images/ROC_Curve.png\", placeholder='Location of PCA image', description='ROC Curve:'),\n",
    "                              )\n",
    "display(exporting) ;"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "12c857be9a419df3da1d5a6b6a8b63faa2c822560ba10bbd2c6bc4fea88a5b77"
  },
  "kernelspec": {
   "display_name": "Python [conda env:mutagenicity_publication] *",
   "language": "python",
   "name": "conda-env-mutagenicity_publication-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  },
  "metadata": {
   "interpreter": {
    "hash": "b09948dd6d10175fac510287364c9510b0542331d735e9f6b8dfd3ade9ddcd48"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
