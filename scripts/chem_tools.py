from tqdm import tqdm
import pandas as pd

from rdkit import Chem, RDLogger
from rdkit.Chem import AllChem

from mol2vec.features import sentences2vec, mol2alt_sentence
from gensim.models import word2vec

lg = RDLogger.logger()
lg.setLevel(RDLogger.CRITICAL)


def mol2vec_fingerprint(smiles_code, model_path='data/input/baseline_model.pkl') :
    
    # Update mol2vec source code according to this GitHub issue: https://github.com/samoturk/mol2vec/issues/14
    # anaconda/envs/YOUR_ENVIRONMENT/lib/python3.7/site-packages/mol2vec/features.py
    output = []
    
    if type(smiles_code) == str : smiles_code = [smiles_code]
    
    # Load existing model
    model = word2vec.Word2Vec.load(model_path)
    
    for i in tqdm(range(len(smiles_code))) :
        try :
            cpd = Chem.MolFromSmiles(smiles_code[i])
            sentence = mol2alt_sentence(cpd, 1)
            vector = sentences2vec([sentence], model)[0]
            
            if len(vector) == 300 :
                output.append(vector)
            else :
                output.append(None)
        
        except :
            output.append(None)

    if len(output) == 1 : return output[0] # If only one SMILES was provided as string

    return output



def molecular_fingerprint(smiles_code, algorithm, **kwargs) :
        
    try :
        molecule = Chem.MolFromSmiles(smiles_code)
    except :
        return None
    
    if molecule is None : return None

    #parameters = itmes()
    if algorithm == 'MACCS' :
        fp_object = list(AllChem.GetMACCSKeysFingerprint(molecule))
        
    elif algorithm == 'ECFP' :
        fp_object = list(AllChem.GetMorganFingerprintAsBitVect(molecule, kwargs['radius'], nBits=kwargs['length']))
        
    elif algorithm == 'FCFP' :
        fp_object = list(AllChem.GetMorganFingerprintAsBitVect(molecule, kwargs['radius'], nBits=kwargs['length'], useFeatures=True))
        
    elif algorithm == 'topological' :
        fp_object = list(Chem.RDKFingerprint(molecule, fpSize=kwargs['length'], maxPath=kwargs['radius'], useHs=False))
     
    elif algorithm == 'layered' :
        fp_object = list(Chem.LayeredFingerprint(molecule, fpSize=kwargs['length'], maxPath=kwargs['radius']))
        
    elif algorithm == 'mol2vec' :
        fp_object = mol2vec_fingerprint(smiles_code)
        
    return fp_object



def batch_molFP(smiles_list, algorithm, **kwargs) :
    """ From a list of SMILES create a list of fingerprint objects. If molecule cannot be processed, then None is returned in list
    Note: MACCS fingerprint do not use fingerprint size and bond order, size of fingerprint will always be 166 bits"""
    
    output = []
    
    # Faster to compute for every smiles at a time, otherwise model is loaded for every SMILES
    if algorithm == 'mol2vec' : 
        output = mol2vec_fingerprint(smiles_list)
        
    else :
        for i in tqdm(range(len(smiles_list))) :
            smiles_code = smiles_list[i]
            molfp = molecular_fingerprint(smiles_code, algorithm, **kwargs)
            output.append(molfp)
                
    return output
