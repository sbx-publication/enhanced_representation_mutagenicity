import pandas as pd
import matplotlib.pyplot as plt

def export_data(data, pred_path, metric_path, figure_path) :
    predictions, results, fig = data
    
    results.to_csv(metric_path, index=False)
    pd.DataFrame(predictions, columns=['SMILES', 'Predictions']).to_csv(pred_path, index=False)
    fig.savefig(figure_path, dpi=300)
    
    print("Results exported!")