from sklearn.metrics import confusion_matrix
from math import sqrt

def basic_metrics(expectations, predictions) :
    tn, fp, fn, tp = confusion_matrix(expectations, predictions).ravel()
    accuracy, sensitivity, specificity = (tp + tn) / (tp+tn+fn+fp), tp / (tp + fn), tn / (tn + fp)
    positive_pred, negative_pred = tp / (tp+fp), tn / (tn+fn)
    MCC = ((tp*tn) - (fp*fn)) / sqrt((tp+fp)*(tp+fn)*(tn+fp)*(tn+fn))
    return {'accuracy' : accuracy,
           'sensitivity' : sensitivity,
           'specificity' : specificity,
           'positive_predictivity' : positive_pred,
           'negative_predictivity' : negative_pred,
           'MCC' : MCC}    