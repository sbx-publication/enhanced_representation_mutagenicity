import random, time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn import svm, metrics
from sklearn.naive_bayes import ComplementNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import KFold

from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.layers import Dense, Dropout, BatchNormalization

from scripts.metrics import basic_metrics



def generate_ANN_model(n_dim, output_dim, layers=[128,64]) :
    """
    input {
        n_dim: int, vector size,
        output_dim: int, dimension of output vector
        layers : list of int, number of neurons available for each layer
    }
    """

    # Initialize model
    model = Sequential()

    # Input layer
    model.add(Dense(layers[0], input_dim=n_dim, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(rate=0.2))
    # Add subsequent layer if provided
    if len(layers) > 1 :
        for layer in layers[1:] :
            model.add(Dense(layer, activation='relu'))
            model.add(BatchNormalization())
            model.add(Dropout(rate=0.2))

    # Add final output layer, sigmoid if binomial, softmax if categorical
    if output_dim == 1 :
        model.add(Dense(output_dim, activation='sigmoid'))
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    else :
        model.add(Dense(output_dim, activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    return model

    
    
def model_train(X, Y, algorithm) :
    
    neurons = (512,256)
    
    print(f"PROCESS MODEL TRAINING {algorithm}: STARTING...")
    if algorithm != 'DNN' :
        if algorithm == 'SVM' :
            classifier = svm.NuSVC(nu=0.2, class_weight='balanced')
        if algorithm == 'NBC' :
            classifier = ComplementNB(alpha=0.5) #MultinomialNB(alpha=0.5)
        if algorithm == 'RF' :
            classifier = RandomForestClassifier(class_weight='balanced')
        classifier.fit(X, Y)
    
    else :
        classifier = generate_ANN_model(X.shape[1], 1, layers=neurons)
        classifier.fit(X, Y, epochs=50, verbose=False, batch_size=512)

    print(f"PROCESS MODEL TRAINING {algorithm}: COMPLETE")
        
    return classifier


def model_assess(X, Y, model, algorithm, matplotlib_axes, n_fold=None) :
    
    print(f"PROCESS MODEL ASSESSMENT")
    
    if 'sklearn' in str(type(model)) :
        viz = metrics.plot_roc_curve(model, X, Y, name='ROC fold {}'.format(n_fold),
                                     alpha=0.6, lw=2, ax=matplotlib_axes)
        fpr, tpr, auc = viz.fpr, viz.tpr, viz.roc_auc                
        predictions = model.predict(X)
        
    else :
       
        predictions = model.predict(X, Y)
        fpr, tpr, thresholds = metrics.roc_curve(Y, predictions, drop_intermediate=True)
        auc = metrics.auc(fpr, tpr)
        matplotlib_axes.plot(fpr, tpr, linewidth=2, alpha=0.6, label=f"ROC fold {n_fold} (AUC = {round(auc, 2)})")
        predictions = [int(round(i[0])) for i in predictions]
    

    # Add to predictions results  
    metric_results = basic_metrics(Y, predictions)
    interp_tpr = np.interp(np.linspace(0, 1, 100), fpr, tpr)
    interp_tpr[0] = 0.0
    
    return predictions, metric_results, interp_tpr, auc
        
        

def pipeline_prediction(data, algorithm, mol_FP=False, mol_properties=False, SA_features=False, DFT_features=False, CV_fold=5) :
    
    assert sum([mol_properties, SA_features, DFT_features]) != 0 or mol_FP, "Select at least one feature"
    
    meta, FPs, properties, SAs, DFTs = data
    input_type = []

    ppty = [FPs, properties, SAs, DFTs]
    ppty_name = ['Structural', 'Mol. Properties', 'Structural Alerts', 'DFT features']
    for i, arg in enumerate([mol_FP, mol_properties, SA_features, DFT_features]) :
        if i == 0:
            tmp = pd.Series(ppty[i][mol_FP].values.tolist(), name=i, index=ppty[i].index)
        else:
            tmp = pd.Series(ppty[i].values.tolist(), name=i, index=ppty[i].index)
        meta = meta.merge(tmp, left_on='SMILES', right_index=True)  
        input_type.append(ppty_name[i])
    
    meta = meta.reset_index(drop=True)
    
    Y = meta['Mutagenicity'].values
    X = np.concatenate([np.stack(meta[colname].values) for colname in meta.columns.difference(['ID', 'Mutagenicity', 'SMILES'])], axis=1)
            
    kfold = KFold(n_splits=CV_fold, shuffle=True)
    
    print(f"Performing pipeline with {' & '.join(input_type)} inputs")
    
    tprs, aucs, predictions = [], [], []
    metric_results = {}
    fig, ax = plt.subplots(figsize=(7,5.5))
    
    for fold_index, data_indices in enumerate(kfold.split(X, Y)) :
        fold_index += 1
        print(f"\nFOLD number: {fold_index}")
        train, test = data_indices
        classifier_model = model_train(X[train], Y[train], algorithm)
        
        fold_predictions, metric_results[fold_index], tpr, auc = model_assess(X[test], Y[test], classifier_model, algorithm, ax, n_fold=fold_index)
        
        aucs.append(auc)
        tprs.append(tpr)
        predictions += list(zip(test, fold_predictions))
 
    # AUC-related calculation
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = metrics.auc(np.linspace(0, 1, 100), mean_tpr)
    std_auc = np.std(aucs)

    # Exporting results
    results = pd.DataFrame.from_dict(metric_results, orient='index')
    results = pd.DataFrame((results.mean(), results.max()), index=['mean', 'max'])
    results['AUC'] = (mean_auc, max(aucs))
    results['std_AUC'] = std_auc
    results['input'] = ' & '.join(input_type)
   
    # Add ROC curve corresponding to randomness
    ax.plot(((0,0),(1,1)), color='#424242', alpha=0.2, linewidth=1.5, linestyle='--')
    #ax.set_title(subset_description)

    if algorithm == 'DNN' :
        ax.set_ylabel('True Positive Rate (Positive label: 1)')
        ax.set_xlabel('False Positive Rate (Positive label: 1)')
        ax.legend(loc=4)

    plt.tight_layout()
    
    data_indices, outcomes = list(zip(*predictions))
    predictions = list(zip(meta['SMILES'].values[list(data_indices)], outcomes))
    
    return predictions, results, fig