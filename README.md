# Machine learning prediction of mutagenicity using enhanced molecular representation #

This repository contains the source code and dataset required to generate the results available in the scientific article *Improving the accuracy of machine learning models for mutagenicity prediction by incorporating novel features*. 


## Requirements  

The following softwares are required to run the application:

* Python 3.7 or above
* [Conda](https://docs.conda.io/en/latest/)
* Web browser (Chrome, Firefox)

## Installation ##

After meeting the software requirements, create a dedicated conda environment in your terminal using the command:

```
conda env create -f environment.yml
```

It will automatically create an environment named `mutagencity_prediction` and  install the required packages for the applications. Then, activate your environment using the command:

```
conda activate mutagencity_prediction
```

You can then launch Jupyter in your web browser with the command:

```
jupyter notebook
```

Open the Notebook `Mutagenicity_Prediction_Pipeline.ipynb`.


## The data sets ##

The original dataset of molecule uses in this study comes from the publication of [Hansen et al.](https://doi.org/10.1021/ci900161g) and is available at the [dedicated website](http://doc.ml.tu-berlin.de/toxbenchmark/). The compounds were standardized (normalize, metal disconnection, reionize) using RDkit, with duplicates removed and largest fragment kept in the case of mixtures. The following packages were used to generated the corresponding features:

* [RDkit](https://www.rdkit.org/): ECFP and MACCS fingerprints.
* [Mol2vec](https://doi.org/10.1021/acs.jcim.7b00616): Word2Vec-based molecular representation.
* [AttentiveFP](https://doi.org/10.1021/acs.jmedchem.9b00959): GCN-based molecular representation.
* [Mordred](https://doi.org/10.1186/s13321-018-0258-y): 1D and 2D molecular properties.
* [ToxAlerts](https://doi.org/10.1021/ci300245q): Structural alerts for genotoxicity, non-genotoxicity and electrophilicities properties.
* [Psi4](https://psicode.org/): Calculation of DFT-based descriptors.

The molecular fingerprints are generated on-the-fly with the exception of GCN that relies on a precomputed model, with the corresponding vectors availables in `data/input/GCN_Vectors.parquet`.

The refined molecules and outcomes (metadata), precalculated molecular descriptors and DFT-based descriptors are also available in the directory `data/input/`.


### About us ###

* [Nicolas Ken Shinada](mailto:shinada@sbx-corp.com)
* [Sucheendra K. Palaniappan](mailto:sucheendra@sbi.jp)
* [The Systems Biology Institute](http://www.sbi.jp/)